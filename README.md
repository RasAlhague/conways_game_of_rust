# Conways Game of Rust:

A in rust written version of Conways Game of Life.
This is just another test to improve my Rust skills and also learn more about [Amethyst.rs](https://book.amethyst.rs/stable/).

## About Conways Game of Life: (Copied from wikipedia)

The Game of Life, also known simply as Life, is a cellular automaton devised by the British mathematician John Horton Conway in 1970.

The game is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input. One interacts with the Game of Life by creating an initial configuration and observing how it evolves, or, for advanced players, by creating patterns with particular properties.

## Rules:

1. Any live cell with fewer than two live neighbours dies, as if by underpopulation.
2. Any live cell with two or three live neighbours lives on to the next generation.
3. Any live cell with more than three live neighbours dies, as if by overpopulation.
4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

# Idea:

 - Writing a simple and fast version of Conways Game of Life in rust
 - no complex rules
 - rendering and co with [Amethyst.rs](https://book.amethyst.rs/stable/)

## Features
 
 - Setting up via commandline arguments.
 - Three cell states: Dead, Alive, Dead after being alive

